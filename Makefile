all: main.cpp
	g++ -Wall -O0 -o main main.cpp -std=c++11

run: all
	./main

clean:
	rm -f main