/*	
 *	Name: 		Cellular automata to approach HIV infection
 *	File name:	main.c
 *	Authors:	Vit Solcik (xsolci00), Cyril Urban (xurban65)
 *	Date:		22. November 2017
 */
#include <stdio.h>
#include <stdlib.h>
#include <random>
#include <iostream>
#include <math.h>
#include "main.hpp"

using namespace std;

cell * lattice_previous[lattice_size][lattice_size];
cell * lattice_new[lattice_size][lattice_size];
cell_count results[WEEKS];
double infected_fraction_start;

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
int print_lattice(bool which_lattice)
{
	int death = 0;
	int a1_count = 0;
	int a2_count = 0;
	int healthy = 0;

	for (int x = 0; x < lattice_size; ++x)
	{
		for (int y = 0; y < lattice_size; ++y)
		{
			if (which_lattice == NEW)
			{
				/* code */
				if (lattice_new[x][y]->state == H)
				{
					cout << "|  H |";
					healthy++;
				}
				else if (lattice_new[x][y]->state == A1)
				{
					cout << "| A1 |";
					a1_count++;
				}
				else if (lattice_new[x][y]->state == A2)
				{
					cout << "| A2 |";
					a2_count++;
				}
				else if (lattice_new[x][y]->state == D)
				{
					cout << "|  D |";
					death++;
				}
			}
			else if (which_lattice == PREVIOUS)
			{
				/* code */
				if (lattice_previous[x][y]->state == H)
				{
					cout << "|  H |";
					healthy++;
				}
				else if (lattice_previous[x][y]->state == A1)
				{
					cout << "| A1 |";
					a1_count++;
				}
				else if (lattice_previous[x][y]->state == A2)
				{
					cout << "| A2 |";
					a2_count++;
				}
				else if (lattice_previous[x][y]->state == D)
				{
					cout << "|  D |";
					death++;
				}
			}
		}

		cout << endl;
	}

	cout << "Healthy: " << healthy << endl;
	cout << "Death: " << death << endl;
	cout << "A1: " << a1_count << endl;
	cout << "A2: " << a2_count << endl;

	return SUCCES;
}

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
cell_count compute_cells()
{
	int death = 0;
	int a1_count = 0;
	int a2_count = 0;
	int healthy = 0;
	int healthy_rtp = 0;
	int healthy_rt = 0;
	int healthy_p = 0;


	cell_count tmp = {0};

	for (int x = 0; x < lattice_size; ++x)
	{
		for (int y = 0; y < lattice_size; ++y)
		{
			if (lattice_new[x][y]->state == H)
			{
				healthy++;
			}
			else if (lattice_new[x][y]->state == A1)
			{
				a1_count++;
			}
			else if (lattice_new[x][y]->state == Hrtp)
			{
				healthy_rtp++;
			}
			else if (lattice_new[x][y]->state == Hrt)
			{
				healthy_rt++;
			}
			else if (lattice_new[x][y]->state == Hp)
			{
				healthy_p++;
			}
			else if (lattice_new[x][y]->state == A2)
			{
				a2_count++;
			}
			else if (lattice_new[x][y]->state == D)
			{
				death++;
			}
		}
	}

	tmp.healthy = healthy + healthy_rtp + healthy_rt + healthy_p;
	tmp.infected = a1_count + a2_count;
	tmp.death = death;

	return tmp;
}

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
int init_cells()
{
	for (int x = 0; x < lattice_size; ++x)
	{
		for (int y = 0; y < lattice_size; ++y)
		{
			lattice_previous[x][y] = (cell*) malloc(sizeof(cell));
			if (lattice_previous[x][y] == NULL)
			{
				cerr << "Error while malloc" << endl;
				exit(ERR);
			}

			lattice_new[x][y] = (cell*) malloc(sizeof(cell));
			if (lattice_new[x][y] == NULL)
			{
				cerr << "Error while malloc" << endl;
				exit(ERR);
			}

			lattice_previous[x][y]->state = H;
			lattice_previous[x][y]->tau = 0;
		}
	}

	return SUCCES;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  a     { parameter_description }
 * @param[in]  b     { parameter_description }
 *
 * @return     { description_of_the_return_value }
 */
int generate_random(int a, int b)
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(a, b);
 
	return dis(gen);
}

/**
 * @brief      { function_description }
 *
 * @param[in]  count  The count
 *
 * @return     { description_of_the_return_value }
 */
int init_hiv_spread(int count)
{
	int x, y;

	for (int i = 0; i < count; ++i)
	{
		x = generate_random(0, lattice_size - 1);
		y = generate_random(0, lattice_size - 1);

		if (lattice_previous[x][y]->state == A1)
		{
			/* Already infected cell */
			i--;
		}
		else
		{
			lattice_previous[x][y]->state = A1;
		}
	}

	return SUCCES;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  weeks  The weeks
 *
 * @return     { description_of_the_return_value }
 */
int run_simulation(int weeks)
{
	double infected_fraction = 0;
	double p_rti = 0;
	double p_pi = 0;
	bool treatment = false;

	for (int i = 0; i < weeks; ++i)
	{
		results[i] = compute_cells();

		infected_fraction = results[i].infected / (double)(lattice_size * lattice_size);

		if (i == START_TREATMENT)
		{
			infected_fraction_start = infected_fraction;
		}

		/* compute effectivness of treatment */
		if (i > START_TREATMENT)
		{
			treatment = true;
			p_rti = compute_retroviral_effectivnes(infected_fraction, P0_RTI);
			p_pi = compute_retroviral_effectivnes(infected_fraction, P0_PI);
		}
		else
		{
			treatment = false;
		}

		cout << i << ": " << p_rti << " " << p_pi <<  " " << infected_fraction << endl;

		lattice_copy(PREVIOUS);
		for (int x = 0; x < lattice_size; ++x)
		{
			for (int y = 0; y < lattice_size; ++y)
			{
				update_cell(x, y, treatment, p_rti, p_pi);
			}
		}
	}

	return SUCCES;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  x     { parameter_description }
 * @param[in]  y     { parameter_description }
 *
 * @return     { description_of_the_return_value }
 */
void find_neighbors(int x, int y, neighbors* tmp)
{

	if (x == 0 && y == 0)
	{
		tmp->down = true;
		tmp->right_middle = true;
		tmp->right_down = true;
	}
	else if (x == 0 && y != 0 && y != lattice_size)
	{
		tmp->up = true;
		tmp->down = true;
		tmp->right_upper = true;
		tmp->right_middle = true;
		tmp->right_down = true;
	}
	else if (x == 0 && y == lattice_size)
	{
		tmp->up = true;
		tmp->right_upper = true;
		tmp->right_middle = true;
	}
	else if (y == 0 && x != 0 && x != lattice_size)
	{
		tmp->down = true;
		tmp->left_middle = true;
		tmp->left_down = true;
		tmp->right_middle = true;
		tmp->right_down = true;
	}
	else if (x == lattice_size && y == 0)
	{
		tmp->left_middle = true;
		tmp->left_down = true;
		tmp->left_down = true;
	}
	else if (x == lattice_size && y != 0 && y != lattice_size)
	{
		tmp->up = true;
		tmp->down = true;
		tmp->left_upper = true;
		tmp->left_middle = true;
		tmp->left_down = true;
	}
	else if (x == lattice_size && y == lattice_size)
	{
		tmp->up = true;
		tmp->left_upper = true;
		tmp->left_middle = true;
	}
	else if (y == lattice_size && x != 0 && x != lattice_size)
	{
		tmp->up = true;
		tmp->left_upper = true;
		tmp->left_middle = true;
		tmp->right_upper = true;
		tmp->right_middle = true;
	}
	else
	{
		tmp->up = true;
		tmp->down = true;
		tmp->left_upper = true;
		tmp->left_middle = true;
		tmp->left_down = true;
		tmp->right_upper = true;
		tmp->right_middle = true;
		tmp->right_down = true;
	}
}

/**
 * @brief      { function_description }
 *
 * @param[in]  x     { parameter_description }
 * @param[in]  y     { parameter_description }
 *
 * @return     { description_of_the_return_value }
 */
int update_cell(int x, int y,bool treatment, double p_rti, double p_pi)
{
	neighbors * cell_neighbors = (neighbors *) malloc(sizeof(neighbors));
	int A2_neighbors_count = 0;
	double probability_H, probability_A1, random_num;

	find_neighbors(x, y, cell_neighbors);

	switch(lattice_previous[x][y]->state)
	{
		case H:

			/* Rule 1c : H -> Hp || Hrt || Hrtp */
			if(treatment == true)
			{
				random_num = generate_random(0, 1000000);

				if (((1 - p_rti) * p_pi * 1000000) > random_num)
				{
					lattice_new[x][y]->state = Hp;
					break;
				}
	
				if (((1 - p_pi) * p_rti * 1000000) > random_num)
				{
					lattice_new[x][y]->state = Hrt;
					break;
				}

				if ((p_rti * p_pi * 1000000) > random_num)
				{
					lattice_new[x][y]->state = Hrtp;
					break;
				}

			}

			/* Rule 1: H -> A1 */
			if (((cell_neighbors->up == true) && (lattice_previous[x][y-1]->state == A1)) ||
				((cell_neighbors->down == true) && (lattice_previous[x][y+1]->state == A1)) ||
				((cell_neighbors->left_upper == true) && (lattice_previous[x-1][y-1]->state == A1)) ||
				((cell_neighbors->left_middle == true) && (lattice_previous[x-1][y]->state == A1)) ||
				((cell_neighbors->left_down == true) && (lattice_previous[x-1][y+1]->state == A1)) ||
				((cell_neighbors->right_upper == true) && (lattice_previous[x+1][y-1]->state == A1)) ||
				((cell_neighbors->right_middle == true) && (lattice_previous[x+1][y]->state == A1)) ||
				((cell_neighbors->right_down == true) && (lattice_previous[x+1][y+1]->state == A1)))
			{
				if(treatment == true)
				{
					random_num = generate_random(0, 1000000);

					if (((1 - p_rti) * (1 - p_pi) * 1000000) > random_num)
					{
						lattice_new[x][y]->state = A1;
						lattice_new[x][y]->tau = 0;
						break;
					}
				}
				else
				{
					lattice_new[x][y]->state = A1;
					lattice_new[x][y]->tau = 0;
					break;
				}
			}

			/* Rule 1: H -> A1 (from A2) */
			if ((cell_neighbors->up == true) && (lattice_previous[x][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->down == true) && (lattice_previous[x][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_upper == true) && (lattice_previous[x-1][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_middle == true) && (lattice_previous[x-1][y]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_down == true) && (lattice_previous[x-1][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_upper == true) && (lattice_previous[x+1][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_middle == true) && (lattice_previous[x+1][y]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_down == true) && (lattice_previous[x+1][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}

			if (A2_neighbors_count > (R-1))
			{
				if(treatment == true)
				{
					random_num = generate_random(0, 1000000);

					if (((1 - p_rti) * (1 - p_pi) * 1000000) > random_num)
					{
						lattice_new[x][y]->state = A1;
						lattice_new[x][y]->tau = 0;
						break;
					}
				}
				else
				{
					lattice_new[x][y]->state = A1;
					lattice_new[x][y]->tau = 0;
					break;
				}
			}

			break;

		/* Rule: A1(TAU weeks old) -> A2 */
		case A1:
			lattice_new[x][y]->tau = lattice_new[x][y]->tau + 1;

			if (lattice_new[x][y]->tau > (TAU-1))
			{
			 	lattice_new[x][y]->state = A2; 
			} 
			break;

		/* Rule: A2 -> D */
		case A2:
			lattice_new[x][y]->state = D;
			break;

		/* Rule -> D -> A1 || H */
		case D:
			probability_H = (1 - P_infec) * P_reg;
			probability_A1 = P_reg * P_infec;

			random_num = generate_random(0, 1000000);

			if (random_num < (probability_A1 * 1000000))
			{
				lattice_new[x][y]->state = A1;
				break;
			}

			if (random_num < (probability_H * 1000000))
			{
				lattice_new[x][y]->state = H;
				break;
			}
			break;

		case Hrt:
			random_num = generate_random(0, 1000000);

			if ((p_pi * 1000000) > random_num)
			{
				lattice_new[x][y]->state = Hrtp;
				break;
			}

			random_num = generate_random(0, 1000000);

			if (((1 - p_pi) * p_rti * 1000000) > random_num)
			{
				lattice_new[x][y]->state = Hrt;
				break;
			}

			if (((cell_neighbors->up == true) && (lattice_previous[x][y-1]->state == A1)) ||
				((cell_neighbors->down == true) && (lattice_previous[x][y+1]->state == A1)) ||
				((cell_neighbors->left_upper == true) && (lattice_previous[x-1][y-1]->state == A1)) ||
				((cell_neighbors->left_middle == true) && (lattice_previous[x-1][y]->state == A1)) ||
				((cell_neighbors->left_down == true) && (lattice_previous[x-1][y+1]->state == A1)) ||
				((cell_neighbors->right_upper == true) && (lattice_previous[x+1][y-1]->state == A1)) ||
				((cell_neighbors->right_middle == true) && (lattice_previous[x+1][y]->state == A1)) ||
				((cell_neighbors->right_down == true) && (lattice_previous[x+1][y+1]->state == A1)))
			{
				if(treatment == true)
				{
					random_num = generate_random(0, 1000000);

					if (((1 - p_rti) * (1 - p_pi) * 1000000) > random_num)
					{
						lattice_new[x][y]->state = A1;
						lattice_new[x][y]->tau = 0;
						break;
					}
				}
				else
				{
					lattice_new[x][y]->state = A1;
					lattice_new[x][y]->tau = 0;
					break;
				}
			}

			if ((cell_neighbors->up == true) && (lattice_previous[x][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->down == true) && (lattice_previous[x][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_upper == true) && (lattice_previous[x-1][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_middle == true) && (lattice_previous[x-1][y]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_down == true) && (lattice_previous[x-1][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_upper == true) && (lattice_previous[x+1][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_middle == true) && (lattice_previous[x+1][y]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_down == true) && (lattice_previous[x+1][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}

			if (A2_neighbors_count > (R-1))
			{
				if(treatment == true)
				{
					random_num = generate_random(0, 1000000);

					if (((1 - p_rti) * (1 - p_pi) * 1000000) > random_num)
					{
						lattice_new[x][y]->state = A1;
						lattice_new[x][y]->tau = 0;
						break;
					}
				}
				else
				{
					lattice_new[x][y]->state = A1;
					lattice_new[x][y]->tau = 0;
					break;
				}
			}

			lattice_new[x][y]->state = H;
			break;

		case Hp:
			random_num = generate_random(0, 1000000);

			if ((p_rti * 1000000) > random_num)
			{
				lattice_new[x][y]->state = Hrtp;
				break;
			}

			random_num = generate_random(0, 1000000);

			if (((1 - p_rti) * p_pi * 1000000) > random_num)
			{
				lattice_new[x][y]->state = Hp;
				break;
			}

			if (((cell_neighbors->up == true) && (lattice_previous[x][y-1]->state == A1)) ||
				((cell_neighbors->down == true) && (lattice_previous[x][y+1]->state == A1)) ||
				((cell_neighbors->left_upper == true) && (lattice_previous[x-1][y-1]->state == A1)) ||
				((cell_neighbors->left_middle == true) && (lattice_previous[x-1][y]->state == A1)) ||
				((cell_neighbors->left_down == true) && (lattice_previous[x-1][y+1]->state == A1)) ||
				((cell_neighbors->right_upper == true) && (lattice_previous[x+1][y-1]->state == A1)) ||
				((cell_neighbors->right_middle == true) && (lattice_previous[x+1][y]->state == A1)) ||
				((cell_neighbors->right_down == true) && (lattice_previous[x+1][y+1]->state == A1)))
			{
				if(treatment == true)
				{
					random_num = generate_random(0, 1000000);

					if (((1 - p_rti) * (1 - p_pi) * 1000000) > random_num)
					{
						lattice_new[x][y]->state = A1;
						lattice_new[x][y]->tau = 0;
						break;
					}
				}
				else
				{
					lattice_new[x][y]->state = A1;
					lattice_new[x][y]->tau = 0;
					break;
				}
			}

			if ((cell_neighbors->up == true) && (lattice_previous[x][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->down == true) && (lattice_previous[x][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_upper == true) && (lattice_previous[x-1][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_middle == true) && (lattice_previous[x-1][y]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->left_down == true) && (lattice_previous[x-1][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_upper == true) && (lattice_previous[x+1][y-1]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_middle == true) && (lattice_previous[x+1][y]->state == A2))
			{
				A2_neighbors_count++;
			}
			if ((cell_neighbors->right_down == true) && (lattice_previous[x+1][y+1]->state == A2))
			{
				A2_neighbors_count++;
			}

			if (A2_neighbors_count > (R-1))
			{
				if(treatment == true)
				{
					random_num = generate_random(0, 1000000);

					if (((1 - p_rti) * (1 - p_pi) * 1000000) > random_num)
					{
						lattice_new[x][y]->state = A1;
						lattice_new[x][y]->tau = 0;
						break;
					}
				}
				else
				{
					lattice_new[x][y]->state = A1;
					lattice_new[x][y]->tau = 0;
					break;
				}
			}

			lattice_new[x][y]->state = H;
			break;

		case Hrtp:
			lattice_new[x][y]->state = H;	
			break;		

	}

	free(cell_neighbors);
	return SUCCES;
}


/**
 * @brief      Calculates the retroviral effectivnes.
 *
 * @param[in]  infected_fraction  The infected fraction
 * @param[in]  p0                 The p 0
 *
 * @return     The retroviral effectivnes.
 */
double compute_retroviral_effectivnes(double infected_fraction, double p0)
{
	double result = 0;
	double BASE, exp, A1, A2;

	cout << " fraction_start: " << infected_fraction_start;

	cout << " fraction: " << infected_fraction;

	BASE = (infected_fraction/infected_fraction_start);
	cout << " BASE: "<< BASE ;

	exp = (2*infected_fraction_start)/(1 - infected_fraction_start);
	cout << " exp: " << exp ;

	A1 = pow(BASE, exp);

	cout << " A1: " << A1 ;

	BASE = (1 - infected_fraction) / (1 - infected_fraction_start);
	cout << " BASE(second): "<< BASE ;

	A2 = pow(BASE, 2);
	cout << " A2: "<< A2 ;

	result = p0 * A1 * A2;
	cout << " result: "<< result << endl ;


	return result;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  direction  The direction
 */
void lattice_copy(bool direction)
{
	for (int x = 0; x < lattice_size; ++x)
	{
		for (int y = 0; y < lattice_size; ++y)
		{
			if (direction == NEW)
			{
				lattice_new[x][y]->state = lattice_previous[x][y]->state;
			}
			if (direction == PREVIOUS)
			{
				lattice_previous[x][y]->state = lattice_new[x][y]->state;
			}
		}
	}
}

/**
 * @brief      Stores results.
 */
void store_results()
{
	FILE *fp1 = fopen("healthy.dat", "w+");
	FILE *fp2 = fopen("infected.dat", "w+");
	FILE *fp3 = fopen("death.dat", "w+");

	double healthy_fraction, infected_fraction, death_fraction;


	if (fp1 == NULL || fp2 == NULL || fp3 == NULL)
	{
		cerr << "Error while creating output files." << endl;
		exit(1);
	}

	for (int i = 0; i < WEEKS; ++i)
	{
		//fprintf(fp, "Week: [ %d ]\t\tHealthy: [ %d ]\t\t Infected: [ %d ]\t\t Death: [ %d ]\n",
		//		i, results[i].healthy, results[i].infected, results[i].death);

		healthy_fraction = results[i].healthy / (double)(lattice_size * lattice_size);
		infected_fraction = results[i].infected / (double)(lattice_size * lattice_size);
		death_fraction = results[i].death / (double)(lattice_size * lattice_size);

		fprintf(fp1, "%d\t%f\n", i, healthy_fraction);
		fprintf(fp2, "%d\t%f\n", i, infected_fraction);
		fprintf(fp3, "%d\t%f\n", i, death_fraction);
	}

    fclose(fp1);
    fclose(fp2);
    fclose(fp3);
}

/**
 * @brief      { function_description }
 */
void free_memory()
{
	for (int x = 0; x < lattice_size; ++x)
	{
		for (int y = 0; y < lattice_size; ++y)
		{
			free(lattice_previous[x][y]);
			free(lattice_new[x][y]);
		}
	}

}

/**
 * @brief      Main
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     { description_of_the_return_value }
 */
int main(int argc, char const *argv[])
{
	error_state err = SUCCES;

	err = init_cells();

	/* Compute number of infected cells on the beginning of simulation */
	int infected_count = (lattice_size * lattice_size) * p_HIV;

	/* Spread infected cells in lattice using uniform distribution */
	err = init_hiv_spread(infected_count);

	/* Copy previous lattice to new */
	lattice_copy(NEW);

	//print_lattice(NEW);

	/* Run simulation */
	err = run_simulation(WEEKS);

	/* Store results to output file */
	store_results();

	/* Free memory */
	free_memory();

	return err;
}