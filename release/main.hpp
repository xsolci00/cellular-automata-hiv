/*	
 *	Name: 		Cellular automata to approach HIV infection
 *	File name:	main.h
 *	Authors:	Vit Solcik (xsolci00), Cyril Urban (xurban65)
 *	Date:		22. November 2017
 */

#define lattice_size 700
#define SUCCES 0
#define ERR 1
#define p_HIV 0.05
#define WEEKS 600
#define PREVIOUS false
#define NEW true
#define TAU 4
#define R 4
#define P_infec 0.00001
#define P_reg 0.99
#define P0_RTI 7.0
#define P0_PI 7.0
#define START_TREATMENT 300


typedef enum cell_state {H, A1, A2, D, Hrtp, Hp, Hrt} cell_state;
typedef int error_state;

typedef struct
{
  cell_state state;
  int tau;
} cell;

typedef struct
{
  int healthy;
  int infected;
  int death;
} cell_count;

typedef struct
{
  bool up;
  bool down;
  bool left;
  bool right;
  bool left_upper;
  bool left_middle;
  bool left_down;
  bool right_upper;
  bool right_middle;
  bool right_down;
} neighbors;

int print_lattice(bool which_lattice);
int init_cells();
int generate_random(int a, int b);
int init_hiv_spread(int count);
void find_neighbors(int x, int y, neighbors *tmp);
int update_cell(int x, int y,bool treatment, double p_rti, double p_pi);
int run_simulation(int weeks);
void lattice_copy(bool direction);
cell_count compute_cells();
void store_results();
double compute_retroviral_effectivnes(double infected_fraction, double p0);
