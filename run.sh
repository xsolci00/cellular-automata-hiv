#!/bin/sh

for i in $(seq 0 20)
do 
	./main
	cd automat_simulation
	mkdir $i
	cd ..
	mv infected.dat automat_simulation/$i/infected.dat
	mv death.dat automat_simulation/$i/death.dat
	mv healthy.dat automat_simulation/$i/healthy.dat
done  

